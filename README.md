# Försäkringsmedicinskt beslutsstöd #

### Om releasen ###

* Version: 1.0
* 2015-06-03 

Oförändrade tjänstekontrakt:

* Inga då detta är första versionen

Nya tjänstekontrakt:

* GetFmb 1.0
* GetVersion 1.0
* GetDiagnosInformation 1.0

Förändrade tjänstekontrakt

* Inga då detta är första versionen

Utgångna tjänstekontrakt

* Inga då detta är första versionen

### Kontaktpersoner ###

* forsakringsmedicinskt.beslutsstod@socialstyrelsen.se

### Om domänen ###
För att effektivisera processen kring sjukskrivningsbedömningar tillgängliggör denna domän den information som ligger till grund för sådana bedömningar i strukturerad form för att kunna integreras i informationssystem. Genom att tillhandahålla information på ett strukturerat sätt ökar användbarheten av den information som Socialstyrelsen idag tillhandahåller i fritext via sin webbplats för försäkringsmedicinskt beslutsstöd (FMB).

Syftet med försäkringsmedicinskt beslutsstöd är att 

* Ge vägledning om vilka informationsmängder som är av vikt vid sjukskrivningsbedömning givet en viss diagnos. 
* Ge ett beslutsunderlag för sjukskrivningsbedömning baserat på de värden som anges för dessa informationsmängder. 
* Ge övrig information om diagnos som inte är kopplat till sjukskrivningsbedömningen men som kan vara till stöd i sjukskrivningsprocessen.